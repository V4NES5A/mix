let equipo = {
    id: 0
}

function setIdEquipo(id) {
    equipo.codigo = id
}

$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    cargarDatos();
    $("#btnGuardar").click(guardar);
    $("#btnEliminar").click(function () {
        eliminar(equipo.codigo)
    });
    $("#btnActualizar").click(modificar);
    $("#btnCancelar").click(reset);
}

function reset() {
    $("#nombre").val(null);
    $("#integrantes").val(null);

    $("#nombre2").val(null);
    $("#integrantes2").val(null);
}

//CARGANDO DATOS A TABLA ESPECIALIDADES
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8080/equipos/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");

            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].codigo + "</strong></td>" +
                    "<td><strong>" + response[i].nombre + "</strong></td>" +
                    "<td><strong>" + response[i].integrantes + "</strong></td>" +
                    "<td><strong>" + response[i].estado + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].codigo +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> <strong>Editar</strong></button>" +
                    "<button onclick='setIdEquipo(" + response[i].codigo +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response) {
    $.ajax({
        url: "http://localhost:8080/equipos/saveOrUpdate",
        method: "Put",
        data: {
            id: null,
            nombre: $("#nombre").val(),
            integrantes: $("#integrantes").val(),
            estado: $("#estado").val()
        },
        success: function () {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function eliminar(id) {
    $.ajax({
        url: "http://localhost:8080/equipos/" + id,
        method: "Delete",
        success: function () {
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8080/equipos/" + id,
        method: "Post",
        success: function (response) {
            $("#id2").val(response.codigo)
            $("#nombre2").val(response.nombre)
            $("#integrantes2").val(response.integrantes)
            $("#estado2").val(response.estado)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}

function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8080/equipos/saveOrUpdate/",
        method: "put",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            integrantes: $("#integrantes2").val(),
            estado: $("#estado2").val()
        },
        success: function (response) {
            console.log(response);
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}