package com.precertificacion.controller;

import java.util.HashMap;
import java.util.List;

import com.precertificacion.model.Equipos;
import com.precertificacion.model.Equipos.Estado;
import com.precertificacion.repository.IEquipoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * EquiposController
 */
@Controller
@RequestMapping("equipos")
@CrossOrigin
public class EquiposController {

    @Autowired
    IEquipoRepository rEquipo;

    // LISTAR
    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Equipos> getAll() {
        return (List<Equipos>) rEquipo.findAll();
    }

    // GUARDAR
    @PutMapping(value = "saveOrUpdate", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public HashMap<String, String> save(@RequestParam Long id, @RequestParam String nombre, @RequestParam Long integrantes,
            @RequestParam Estado estado) {

        Equipos equipo = new Equipos();
        System.out.println(id);
        if (id != null) {
            equipo = new Equipos(id, nombre, integrantes, estado);
        } else if (id == null) {
            id = rEquipo.count() + 3;
            equipo = new Equipos(id, nombre, integrantes, estado);
        }

        HashMap<String, String> jsonReturn = new HashMap<>();

        try {
            rEquipo.save(equipo);

            jsonReturn.put("Estado", "OK");
            jsonReturn.put("Mensaje", "Registro guardado");

            return jsonReturn;
        } catch (Exception e) {

            jsonReturn.put("Estado", "Error");
            jsonReturn.put("Mensaje", "Registro no guardado");

            return jsonReturn;
        }
    }
    
    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Equipos getMethodName(@PathVariable Long id) {
        return rEquipo.findById(id).get();
    }

    /** Metodo_para_eliminar_registro */
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
        Equipos e = rEquipo.findById(id).get();
        rEquipo.delete(e);
        return true;
    }
}