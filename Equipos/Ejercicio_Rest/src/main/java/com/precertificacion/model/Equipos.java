package com.precertificacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * equipos
 */
@Entity
@Table(name = "equipos")
public class Equipos {

    public enum Estado {
        Activo, Inactivo
    }

    @Id
    @Column(nullable = false, length = 30, columnDefinition = "char")
    private Long codigo;

    private String nombre;

    private Long integrantes;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 30, columnDefinition = "ENUM('Activo', 'Inactivo')")
    private Estado estado;

    public Equipos() {
        
    };

    public Equipos(Long codigo, String nombre, Long integrantes, Estado estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.integrantes = integrantes;
        this.estado = estado;
    };

    public Equipos(String nombre, Long integrantes, Estado estado) {
        this.nombre = nombre;
        this.integrantes = integrantes;
        this.estado = estado;
    };

    public Long getCodigo() {
        return this.codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getIntegrantes() {
        return this.integrantes;
    }

    public void setIntegrantes(Long integrantes) {
        this.integrantes = integrantes;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}