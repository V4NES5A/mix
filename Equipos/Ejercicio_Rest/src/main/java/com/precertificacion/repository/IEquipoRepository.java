package com.precertificacion.repository;

import com.precertificacion.model.Equipos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * IEquipoRepository
 */
@Repository
public interface IEquipoRepository extends CrudRepository<Equipos, Long>{
 
}