package com.personal.proyecto.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.proyecto.model.Carro;
import com.sun.xml.bind.v2.model.core.ID;

@Repository
public interface ICarroRepository extends CrudRepository<Carro, Long>{

}
