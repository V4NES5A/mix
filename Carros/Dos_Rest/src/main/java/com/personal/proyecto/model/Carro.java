package com.personal.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "carros")
public class Carro {
	public enum Estado {
        Bueno, Chocado
    }
	 @Id
	    @Column(nullable = false, length = 30, columnDefinition = "char")
	    private Long id;
	 
	 	private String modelo;
	 	private String marca;
	 	
	 	@Enumerated(EnumType.STRING)
	    @Column(nullable = false, length = 30, columnDefinition = "ENUM('Bueno', 'Chocado')")
	    private Estado estado;

		public Carro() {
		}

		public Carro(Long id, String modelo, String marca, Estado estado) {
			super();
			this.id = id;
			this.modelo = modelo;
			this.marca = marca;
			this.estado = estado;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getModelo() {
			return modelo;
		}

		public void setModelo(String modelo) {
			this.modelo = modelo;
		}

		public String getMarca() {
			return marca;
		}

		public void setMarca(String marca) {
			this.marca = marca;
		}

		public Estado getEstado() {
			return estado;
		}

		public void setEstado(Estado estado) {
			this.estado = estado;
		}
}
