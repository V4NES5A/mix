package com.personal.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DosRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DosRestApplication.class, args);
	}

}
