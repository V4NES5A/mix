package com.personal.proyecto.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.proyecto.model.Carro;
import com.personal.proyecto.model.Carro.Estado;
import com.personal.proyecto.repository.ICarroRepository;

@Controller
@RequestMapping("carros")
@CrossOrigin
public class CarroController {
	@Autowired
	ICarroRepository rCarro;
	
	
	 // LISTAR
    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Carro> getAll() {
        return (List<Carro>) rCarro.findAll();
    }
    
 // GUARDAR
    @PutMapping(value = "saveOrUpdate", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public HashMap<String, String> save(@RequestParam Long id,@RequestParam String modelo, @RequestParam String marca,@RequestParam Estado estado) {

        Carro carro = new Carro();
        
        System.out.println(id);
        if (id != null) {
            carro = new Carro(id, modelo, marca, estado);
        } else if (id == null) {
            id = rCarro.count() + 3;
            carro = new Carro(id, modelo, marca, estado);
        }

        HashMap<String, String> jsonReturn = new HashMap<>();

        try {
            rCarro.save(carro);

            jsonReturn.put("Estado", "OK");
            jsonReturn.put("Mensaje", "Registro guardado");

            return jsonReturn;
        } catch (Exception e) {

            jsonReturn.put("Estado", "Error");
            jsonReturn.put("Mensaje", "Registro no guardado");

            return jsonReturn;
        }
    }
    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Carro getMethodName(@PathVariable Long id) {
        return rCarro.findById(id).get();
    }

    /** Metodo_para_eliminar_registro */
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
        Carro e = rCarro.findById(id).get();
        rCarro.delete(e);
        return true;
    }

}
